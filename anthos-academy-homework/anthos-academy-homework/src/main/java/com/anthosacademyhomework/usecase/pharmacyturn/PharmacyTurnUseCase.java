package com.anthosacademyhomework.usecase.pharmacyturn;

import com.anthosacademyhomework.resource.datasource.countrypharmacy.model.CountryPharmacyModel;
import com.anthosacademyhomework.resource.datasource.excepcion.ExceptionGenericDataSource;
import com.anthosacademyhomework.resource.datasource.pharmacyturn.model.PharmacyTurnModel;
import com.anthosacademyhomework.resource.repository.countrypharmacy.CountryPharmacyRepository;
import com.anthosacademyhomework.resource.repository.pharmacyturn.PharmacyTurnRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PharmacyTurnUseCase {
    @Autowired
    private PharmacyTurnRepository pharmacyTurnRepository;

    public List<PharmacyTurnModel> pharmacyTurnModels() throws ExceptionGenericDataSource {
        return pharmacyTurnRepository.pharmacyEspecif();
    }

    }
