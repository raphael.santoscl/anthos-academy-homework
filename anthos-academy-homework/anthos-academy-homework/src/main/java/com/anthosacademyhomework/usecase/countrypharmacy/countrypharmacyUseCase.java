package com.anthosacademyhomework.usecase.countrypharmacy;

import com.anthosacademyhomework.resource.datasource.countrypharmacy.model.CountryPharmacyModel;
import com.anthosacademyhomework.resource.datasource.excepcion.ExceptionGenericDataSource;
import com.anthosacademyhomework.resource.repository.countrypharmacy.CountryPharmacyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class countrypharmacyUseCase {

    @Autowired
    private CountryPharmacyRepository getPharmacyValue;

    public List<CountryPharmacyModel> getCountryPharmacyModel() throws ExceptionGenericDataSource {
        return  getPharmacyValue.getAllEPharmacy();


    }

}
