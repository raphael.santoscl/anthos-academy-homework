package com.anthosacademyhomework.usecase.alltaskmoreeconomic;

import com.anthosacademyhomework.resource.datasource.createtask.entity.CreateTaskEntity;
import com.anthosacademyhomework.resource.datasource.economicvalue.model.EconomicValueModel;
import com.anthosacademyhomework.resource.datasource.excepcion.ExceptionGenericDataSource;
import com.anthosacademyhomework.resource.repository.economicvalue.EconomicValueRepository;
import com.anthosacademyhomework.resource.repository.searchalltasks.SearchAllTaskRepository;
import com.anthosacademyhomework.usecase.alltaskmoreeconomic.mapper.TaskEconomicIndicatorToAllTaskEconomicIndicatorModelMapper;
import com.anthosacademyhomework.usecase.alltaskmoreeconomic.model.AllTaskEconomicIndicatorModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AllTaskMoreEconomicUseCase {

    @Autowired
    SearchAllTaskRepository gettTaskRepository;

    @Autowired
    EconomicValueRepository economicValueRepository;

    @Autowired
    TaskEconomicIndicatorToAllTaskEconomicIndicatorModelMapper getAllTaskIndicator;

    public AllTaskEconomicIndicatorModel allTaskEconomicIndicatorModel() throws ExceptionGenericDataSource {

        List< CreateTaskEntity> createTaskEntityList= gettTaskRepository.getAllTask();
        EconomicValueModel economicValueModel= economicValueRepository.getAllEconomic();
        AllTaskEconomicIndicatorModel allTaskEconomicIndicatorModel=  getAllTaskIndicator.mapper(createTaskEntityList, economicValueModel );

        return allTaskEconomicIndicatorModel;
    }





}
