package com.anthosacademyhomework.usecase.alltaskmoreeconomic.model;

import com.anthosacademyhomework.resource.datasource.createtask.entity.CreateTaskEntity;
import com.anthosacademyhomework.resource.datasource.economicvalue.model.EconomicValueModel;

import java.util.List;

public class AllTaskEconomicIndicatorModel {

    private List<CreateTaskEntity> createTaskEntities ;
    private EconomicValueModel economicValueModel;

    public List<CreateTaskEntity> getCreateTaskEntities() {
        return createTaskEntities;
    }

    public EconomicValueModel getEconomicValueModel() {
        return economicValueModel;
    }

    public static AllTaskEconomicIndicatorModelBuilder builder() {
        return new AllTaskEconomicIndicatorModelBuilder();
    }
    public static final class AllTaskEconomicIndicatorModelBuilder {
        private List<CreateTaskEntity> createTaskEntities;
        private EconomicValueModel economicValueModel;

        private AllTaskEconomicIndicatorModelBuilder() {
        }



        public AllTaskEconomicIndicatorModelBuilder withCreateTaskEntities(List<CreateTaskEntity> createTaskEntities) {
            this.createTaskEntities = createTaskEntities;
            return this;
        }

        public AllTaskEconomicIndicatorModelBuilder withEconomicValueModel(EconomicValueModel economicValueModel) {
            this.economicValueModel = economicValueModel;
            return this;
        }

        public AllTaskEconomicIndicatorModel build() {
            AllTaskEconomicIndicatorModel allTaskEconomicIndicatorModel = new AllTaskEconomicIndicatorModel();
            allTaskEconomicIndicatorModel.economicValueModel = this.economicValueModel;
            allTaskEconomicIndicatorModel.createTaskEntities = this.createTaskEntities;
            return allTaskEconomicIndicatorModel;
        }
    }
}
