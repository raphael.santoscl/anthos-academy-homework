package com.anthosacademyhomework.usecase.deletetask;

import com.anthosacademyhomework.resource.repository.deletetask.DeleteTaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DeleteTaskUseCase {

    @Autowired
    DeleteTaskRepository deleteTaskRepository;

public void DeleteTask (Long id){
    deleteTaskRepository.deletetask(id);
}
}
