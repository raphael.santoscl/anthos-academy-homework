package com.anthosacademyhomework.usecase.pharmacyespecific;

import com.anthosacademyhomework.resource.datasource.countrypharmacy.model.CountryPharmacyModel;
import com.anthosacademyhomework.resource.datasource.excepcion.ExceptionGenericDataSource;
import com.anthosacademyhomework.resource.repository.pharmacyespecific.PharmacyEspecifRepository;
import com.anthosacademyhomework.resource.repository.pharmacyespecific.impl.PharmacyEspecificRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PharmacyEspecificUse {

    @Autowired
    PharmacyEspecificRepositoryImpl getPharmacyEspecific;

    public List<CountryPharmacyModel> getPharmacyEspecific() throws ExceptionGenericDataSource {
        return getPharmacyEspecific.pharmacyEspecif();
    }
}
