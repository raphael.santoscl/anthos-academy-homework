package com.anthosacademyhomework.usecase.alltaskmoreeconomic.mapper;

import com.anthosacademyhomework.resource.datasource.createtask.entity.CreateTaskEntity;
import com.anthosacademyhomework.resource.datasource.economicvalue.model.EconomicValueModel;
import com.anthosacademyhomework.usecase.alltaskmoreeconomic.model.AllTaskEconomicIndicatorModel;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public class TaskEconomicIndicatorToAllTaskEconomicIndicatorModelMapper {

    public AllTaskEconomicIndicatorModel mapper(List<CreateTaskEntity> listEconomic, EconomicValueModel economicValueModel){

        return AllTaskEconomicIndicatorModel.builder()
                .withCreateTaskEntities(listEconomic)
                .withEconomicValueModel(economicValueModel)
                .build();
    }

}
