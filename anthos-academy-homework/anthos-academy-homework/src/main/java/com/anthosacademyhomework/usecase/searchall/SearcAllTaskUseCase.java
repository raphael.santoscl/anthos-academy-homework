package com.anthosacademyhomework.usecase.searchall;

import com.anthosacademyhomework.resource.datasource.createtask.entity.CreateTaskEntity;
import com.anthosacademyhomework.resource.repository.searchalltasks.SearchAllTaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SearcAllTaskUseCase {

    @Autowired
    private SearchAllTaskRepository searchAllTask;

    public List<CreateTaskEntity> searchaAllTask (){

        return searchAllTask.getAllTask();

    }
}