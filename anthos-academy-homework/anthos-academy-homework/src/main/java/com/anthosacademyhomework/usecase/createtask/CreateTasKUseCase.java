package com.anthosacademyhomework.usecase.createtask;

import com.anthosacademyhomework.resource.datasource.createtask.entity.CreateTaskEntity;
import com.anthosacademyhomework.resource.repository.createtask.CreateTaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CreateTasKUseCase {

@Autowired
private CreateTaskRepository guardarTaskRepository;

public void execute (CreateTaskEntity createTask){

    guardarTaskRepository.saveCreateTask( createTask);

}

}
