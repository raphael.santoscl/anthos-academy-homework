package com.anthosacademyhomework.usecase.pharmacyturnespecifc;

import com.anthosacademyhomework.resource.datasource.countrypharmacy.model.CountryPharmacyModel;
import com.anthosacademyhomework.resource.datasource.excepcion.ExceptionGenericDataSource;
import com.anthosacademyhomework.resource.datasource.pharmacyturn.model.PharmacyTurnModel;
import com.anthosacademyhomework.resource.repository.pharmacyespecific.impl.PharmacyEspecificRepositoryImpl;
import com.anthosacademyhomework.resource.repository.pharmacyturnespefic.impl.PharmacyTurnEspecificRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PharmacyTurnEspecificUseCase {
    @Autowired
    PharmacyTurnEspecificRepositoryImpl getPharmacyEspecific;

    public List<PharmacyTurnModel> getPharmacyEspecific() throws ExceptionGenericDataSource {
        return getPharmacyEspecific.pharmacyEspecif();
    }


}
