package com.anthosacademyhomework.usecase.feriado;

import com.anthosacademyhomework.resource.datasource.excepcion.ExceptionGenericDataSource;
import com.anthosacademyhomework.resource.datasource.feriado.model.FeriadoValueModel;
import com.anthosacademyhomework.resource.repository.feriado.FeriadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FeriadoUseCase {

    @Autowired
    FeriadoRepository feriadoRepository;

    public FeriadoValueModel getFeriado() throws ExceptionGenericDataSource {
        return feriadoRepository.getAlLFeriado();
    }

}
