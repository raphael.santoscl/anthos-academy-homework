package com.anthosacademyhomework.usecase.updatetask;

import com.anthosacademyhomework.resource.datasource.createtask.entity.CreateTaskEntity;
import com.anthosacademyhomework.resource.repository.updatetask.UpdateTaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UpdateTaskUseCase {

    @Autowired
    UpdateTaskRepository updateTaskRepository;

    public void execute (CreateTaskEntity updateTask){

        updateTaskRepository.saveUpdateCreateTask(updateTask);


    }
    }



