package com.anthosacademyhomework.resource.datasource.pharmacyturn;

import com.anthosacademyhomework.resource.datasource.excepcion.ExceptionGenericDataSource;
import com.anthosacademyhomework.resource.datasource.pharmacyturn.model.PharmacyTurnModel;

import java.util.List;

public interface PharmacyTurnDataSource {
    List<PharmacyTurnModel> pharmavalue() throws ExceptionGenericDataSource;
}
