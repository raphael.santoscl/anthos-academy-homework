package com.anthosacademyhomework.resource.datasource.economicvalue.impl;

import com.anthosacademyhomework.resource.configuration.ApiConfig;
import com.anthosacademyhomework.resource.datasource.economicvalue.EconomicValueDataSource;
import com.anthosacademyhomework.resource.datasource.economicvalue.model.EconomicValueModel;
import com.anthosacademyhomework.resource.datasource.excepcion.ExceptionGenericDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;


@Component
public class EconomicValueDataSourceImpl implements EconomicValueDataSource {

    @Autowired
    ApiConfig ApiConfig;


    @Override
    public EconomicValueModel obtenerValoresEcon() throws ExceptionGenericDataSource {
try {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    HttpEntity requestHttpEntity = new HttpEntity(headers);
    ResponseEntity<EconomicValueModel> response = ApiConfig.restTemplate().exchange("https://www.mindicador.cl/api", HttpMethod.GET, requestHttpEntity, EconomicValueModel.class);

    return response.getBody();
}catch (Exception e){
    throw new ExceptionGenericDataSource("Falla en el Servicio de Valores Financieros");
}
    }
}
