package com.anthosacademyhomework.resource.datasource.excepcion;

public class ExceptionDataSource extends RuntimeException{
    public ExceptionDataSource(String message, Throwable cause) {
        super(message, cause);
    }
}