package com.anthosacademyhomework.resource.repository.createtask.impl;

import com.anthosacademyhomework.resource.datasource.createtask.CreateTaskDAO;
import com.anthosacademyhomework.resource.datasource.createtask.entity.CreateTaskEntity;
import com.anthosacademyhomework.resource.repository.createtask.CreateTaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CreateTaskRepositoryImpl implements CreateTaskRepository {
    @Autowired
    private CreateTaskDAO saveCreateTask;

    @Override
    public void saveCreateTask(CreateTaskEntity createTask) {

        saveCreateTask.save(createTask);

    }
}
