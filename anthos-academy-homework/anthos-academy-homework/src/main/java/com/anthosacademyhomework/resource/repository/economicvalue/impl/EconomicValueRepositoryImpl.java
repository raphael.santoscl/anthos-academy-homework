package com.anthosacademyhomework.resource.repository.economicvalue.impl;

import com.anthosacademyhomework.resource.datasource.economicvalue.EconomicValueDataSource;
import com.anthosacademyhomework.resource.datasource.economicvalue.model.EconomicValueModel;
import com.anthosacademyhomework.resource.datasource.excepcion.ExceptionGenericDataSource;
import com.anthosacademyhomework.resource.repository.economicvalue.EconomicValueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Repository
public class EconomicValueRepositoryImpl implements EconomicValueRepository {

    @Autowired
    EconomicValueDataSource economicValueDataSource;

    @Override
    public EconomicValueModel getAllEconomic() throws ExceptionGenericDataSource {
        return economicValueDataSource.obtenerValoresEcon();


    }

}

