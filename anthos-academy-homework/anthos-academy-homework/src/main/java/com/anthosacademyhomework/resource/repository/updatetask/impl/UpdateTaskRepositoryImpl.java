package com.anthosacademyhomework.resource.repository.updatetask.impl;

import com.anthosacademyhomework.resource.datasource.createtask.CreateTaskDAO;
import com.anthosacademyhomework.resource.datasource.createtask.entity.CreateTaskEntity;
import com.anthosacademyhomework.resource.repository.updatetask.UpdateTaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UpdateTaskRepositoryImpl implements UpdateTaskRepository {
    @Autowired
    private CreateTaskDAO saveUpdateCreateTask;

    @Override
    public void saveUpdateCreateTask(CreateTaskEntity updateTask) {

        saveUpdateCreateTask.save(updateTask);

    }
}
