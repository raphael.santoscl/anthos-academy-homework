package com.anthosacademyhomework.resource.repository.pharmacyturn.impl;

import com.anthosacademyhomework.resource.datasource.countrypharmacy.CountryPharmacyDataSource;
import com.anthosacademyhomework.resource.datasource.excepcion.ExceptionGenericDataSource;
import com.anthosacademyhomework.resource.datasource.pharmacyturn.PharmacyTurnDataSource;
import com.anthosacademyhomework.resource.datasource.pharmacyturn.model.PharmacyTurnModel;
import com.anthosacademyhomework.resource.repository.pharmacyturn.PharmacyTurnRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PharmacyTurnRepositoryImpl implements PharmacyTurnRepository {

    @Autowired
    PharmacyTurnDataSource pharmacyTurnDataSource;

    @Override
    public List<PharmacyTurnModel> pharmacyEspecif() throws ExceptionGenericDataSource {



        return pharmacyTurnDataSource.pharmavalue();
    }
}
