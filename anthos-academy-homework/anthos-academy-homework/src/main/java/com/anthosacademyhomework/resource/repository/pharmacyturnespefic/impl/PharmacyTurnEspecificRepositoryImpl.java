package com.anthosacademyhomework.resource.repository.pharmacyturnespefic.impl;

import com.anthosacademyhomework.resource.datasource.excepcion.ExceptionGenericDataSource;
import com.anthosacademyhomework.resource.datasource.pharmacyturn.PharmacyTurnDataSource;
import com.anthosacademyhomework.resource.datasource.pharmacyturn.model.PharmacyTurnModel;
import com.anthosacademyhomework.resource.repository.pharmacyturn.PharmacyTurnRepository;
import com.anthosacademyhomework.resource.repository.pharmacyturnespefic.PharmacyTurnEspecificRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class PharmacyTurnEspecificRepositoryImpl implements PharmacyTurnEspecificRepository {

    @Autowired
    PharmacyTurnDataSource pharmacyTurnDataSource;

    @Override
    public List<PharmacyTurnModel> pharmacyEspecif() throws ExceptionGenericDataSource {
        List<PharmacyTurnModel> listFilter=new ArrayList<>();
        List<PharmacyTurnModel> listPharmacy = pharmacyTurnDataSource.pharmavalue();

        for (PharmacyTurnModel cont:listPharmacy
        ) {
            if (cont.getComunaNombre().equals("CHIGUAYANTE") || cont.getComunaNombre().equals("CURANILAHUE")|| cont.getComunaNombre().equals("LAS CABRAS")|| cont.getComunaNombre().equals("RIO NEGRO")|| cont.getComunaNombre().equals("TOCOPILLA")){
                listFilter.add(cont);
            }

        }


        return listFilter;
    }
}
