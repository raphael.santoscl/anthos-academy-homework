package com.anthosacademyhomework.resource.datasource.feriado.impl;

import com.anthosacademyhomework.resource.configuration.ApiConfig;
import com.anthosacademyhomework.resource.datasource.countrypharmacy.model.CountryPharmacyModel;
import com.anthosacademyhomework.resource.datasource.excepcion.ExceptionDataSource;
import com.anthosacademyhomework.resource.datasource.excepcion.ExceptionGenericDataSource;
import com.anthosacademyhomework.resource.datasource.excepcion.HttpClientExceptionDataSource;
import com.anthosacademyhomework.resource.datasource.feriado.FeriadoDataSource;
import com.anthosacademyhomework.resource.datasource.feriado.model.DetalleFeriadoModel;
import com.anthosacademyhomework.resource.datasource.feriado.model.FeriadoValueModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;

@Component
class FeriadoDataSourceImpl implements FeriadoDataSource {

    @Autowired
    ApiConfig ApiConfig;
    String urlFarmacia = "https://api.victorsanmartin.com/feriados/en.json";

    @Override
    public FeriadoValueModel feriadovalue() throws ExceptionGenericDataSource {
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            HttpEntity requestHttpEntity = new HttpEntity(headers);

            // Realiza la llamada utilizando el método exchange
            ResponseEntity<FeriadoValueModel> responseEntity = ApiConfig.restTemplate().exchange(
                    urlFarmacia,
                    HttpMethod.GET,
                    requestHttpEntity,
                    FeriadoValueModel.class
            );

            return responseEntity.getBody();

        } catch (HttpClientErrorException ex) {
            throw new HttpClientExceptionDataSource(ex.getStatusCode(), ex.getMessage());
        } catch (Exception e) {
            throw new ExceptionDataSource("Error al consumir servicio todas las farmacias de chile", e.getCause());
        }
    }
}