package com.anthosacademyhomework.resource.repository.deletetask.impl;

import com.anthosacademyhomework.resource.datasource.createtask.CreateTaskDAO;
import com.anthosacademyhomework.resource.repository.deletetask.DeleteTaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DeleteTaskRepositoryImpl implements DeleteTaskRepository {

    @Autowired
    CreateTaskDAO deleteTask1;


    @Override
    public void deletetask(Long id) {
        deleteTask1.deleteById(id);
    }
}
