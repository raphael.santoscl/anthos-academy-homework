package com.anthosacademyhomework.resource.repository.createtask;

import com.anthosacademyhomework.resource.datasource.createtask.entity.CreateTaskEntity;

public interface CreateTaskRepository {
    void saveCreateTask(CreateTaskEntity createTask);
}
