package com.anthosacademyhomework.resource.repository.feriado.impl;

import com.anthosacademyhomework.resource.datasource.excepcion.ExceptionGenericDataSource;
import com.anthosacademyhomework.resource.datasource.feriado.FeriadoDataSource;
import com.anthosacademyhomework.resource.datasource.feriado.model.FeriadoValueModel;
import com.anthosacademyhomework.resource.repository.feriado.FeriadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class FeriadoRepositoryImpl implements FeriadoRepository {
    @Autowired
    FeriadoDataSource feriadoDataSource;

    @Override
    public FeriadoValueModel getAlLFeriado() throws ExceptionGenericDataSource {
        return feriadoDataSource.feriadovalue();
    }
}
