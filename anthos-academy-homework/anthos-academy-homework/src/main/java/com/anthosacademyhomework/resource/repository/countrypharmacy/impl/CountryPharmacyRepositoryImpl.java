package com.anthosacademyhomework.resource.repository.countrypharmacy.impl;

import com.anthosacademyhomework.resource.datasource.countrypharmacy.CountryPharmacyDataSource;
import com.anthosacademyhomework.resource.datasource.countrypharmacy.model.CountryPharmacyModel;
import com.anthosacademyhomework.resource.datasource.excepcion.ExceptionGenericDataSource;
import com.anthosacademyhomework.resource.repository.countrypharmacy.CountryPharmacyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CountryPharmacyRepositoryImpl implements CountryPharmacyRepository {
    @Autowired
    CountryPharmacyDataSource countryPharmacyDataSource;


    @Override
    public List<CountryPharmacyModel> getAllEPharmacy() throws ExceptionGenericDataSource {


        //return countryPharmacyDataSource.pharmavalue();
     return countryPharmacyDataSource.pharmavalue();
    }
}
