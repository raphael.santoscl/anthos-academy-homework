package com.anthosacademyhomework.resource.datasource.countrypharmacy.impl;

import com.anthosacademyhomework.resource.configuration.ApiConfig;
import com.anthosacademyhomework.resource.datasource.countrypharmacy.CountryPharmacyDataSource;
import com.anthosacademyhomework.resource.datasource.countrypharmacy.model.CountryPharmacyModel;
import com.anthosacademyhomework.resource.datasource.excepcion.ExceptionDataSource;
import com.anthosacademyhomework.resource.datasource.excepcion.HttpClientExceptionDataSource;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;

@Component
public class CountryPharmacyImpl implements CountryPharmacyDataSource {

    @Autowired
    ApiConfig ApiConfig;
    String urlFarmacia = "https://midas.minsal.cl/farmacia_v2/WS/getLocales.php";

    @Override
    public List<CountryPharmacyModel> pharmavalue() {
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            HttpEntity requestHttpEntity = new HttpEntity(headers);

            // Realiza la llamada utilizando el método exchange
            ResponseEntity<String> responseEntity = ApiConfig.restTemplate().exchange(
                    urlFarmacia,
                    HttpMethod.GET,
                    requestHttpEntity,
                    String.class
            );

            // Obtiene la respuesta en formato HTML
            String htmlResponse = responseEntity.getBody();

            return List.of(objectMapper.readValue(htmlResponse, CountryPharmacyModel[].class));

        } catch (HttpClientErrorException ex) {
            throw new HttpClientExceptionDataSource(ex.getStatusCode(), ex.getMessage());
        } catch (Exception e) {
            throw new ExceptionDataSource("Error al consumir servicio todas las farmacias de chile", e.getCause());
        }
    }
}