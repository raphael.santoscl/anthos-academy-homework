package com.anthosacademyhomework.resource.datasource.feriado;

import com.anthosacademyhomework.resource.datasource.countrypharmacy.model.CountryPharmacyModel;
import com.anthosacademyhomework.resource.datasource.excepcion.ExceptionGenericDataSource;
import com.anthosacademyhomework.resource.datasource.feriado.model.DetalleFeriadoModel;
import com.anthosacademyhomework.resource.datasource.feriado.model.FeriadoValueModel;

import java.util.List;

public interface FeriadoDataSource {

    FeriadoValueModel feriadovalue() throws ExceptionGenericDataSource;
}
