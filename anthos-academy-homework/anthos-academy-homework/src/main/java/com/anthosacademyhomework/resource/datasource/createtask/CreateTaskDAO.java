package com.anthosacademyhomework.resource.datasource.createtask;

import com.anthosacademyhomework.resource.datasource.createtask.entity.CreateTaskEntity;
import org.springframework.data.repository.CrudRepository;

public interface CreateTaskDAO extends CrudRepository<CreateTaskEntity, Long>  {
}
