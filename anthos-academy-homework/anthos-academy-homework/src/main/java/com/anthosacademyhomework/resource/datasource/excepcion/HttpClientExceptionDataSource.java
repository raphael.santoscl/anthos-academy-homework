package com.anthosacademyhomework.resource.datasource.excepcion;


import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

public class HttpClientExceptionDataSource extends HttpClientErrorException {
        public HttpClientExceptionDataSource(HttpStatus httpStatus, String message) {
            super(httpStatus, message);
        }
    }

