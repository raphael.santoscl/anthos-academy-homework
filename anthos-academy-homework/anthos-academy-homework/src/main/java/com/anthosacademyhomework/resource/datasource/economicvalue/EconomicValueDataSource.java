package com.anthosacademyhomework.resource.datasource.economicvalue;

import com.anthosacademyhomework.resource.datasource.economicvalue.model.EconomicValueModel;
import com.anthosacademyhomework.resource.datasource.excepcion.ExceptionGenericDataSource;

public interface EconomicValueDataSource {

    EconomicValueModel obtenerValoresEcon() throws ExceptionGenericDataSource;

}
