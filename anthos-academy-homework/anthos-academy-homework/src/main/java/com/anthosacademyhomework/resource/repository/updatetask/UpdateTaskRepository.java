package com.anthosacademyhomework.resource.repository.updatetask;

import com.anthosacademyhomework.resource.datasource.createtask.entity.CreateTaskEntity;

public interface UpdateTaskRepository {
    void saveUpdateCreateTask(CreateTaskEntity createTask);

}
