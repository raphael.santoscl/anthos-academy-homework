package com.anthosacademyhomework.resource.datasource.feriado.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FeriadoValueModel implements Serializable {

    @JsonProperty("status")
    private String status;
    @JsonProperty("data")
private List<DetalleFeriadoModel> data;

    public FeriadoValueModel() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<DetalleFeriadoModel> getData() {
        return data;
    }

    public void setData(List<DetalleFeriadoModel> data) {
        this.data = data;
    }
}
