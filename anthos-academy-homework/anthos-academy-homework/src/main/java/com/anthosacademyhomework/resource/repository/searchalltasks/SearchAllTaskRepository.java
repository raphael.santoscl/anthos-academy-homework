package com.anthosacademyhomework.resource.repository.searchalltasks;

import com.anthosacademyhomework.resource.datasource.createtask.entity.CreateTaskEntity;

import java.util.List;

public interface SearchAllTaskRepository {
    List<CreateTaskEntity> getAllTask();
}
