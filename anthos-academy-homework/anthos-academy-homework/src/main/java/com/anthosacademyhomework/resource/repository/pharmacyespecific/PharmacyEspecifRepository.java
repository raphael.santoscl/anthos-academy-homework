package com.anthosacademyhomework.resource.repository.pharmacyespecific;

import com.anthosacademyhomework.resource.datasource.countrypharmacy.model.CountryPharmacyModel;
import com.anthosacademyhomework.resource.datasource.excepcion.ExceptionGenericDataSource;

import java.util.List;

public interface PharmacyEspecifRepository {
    public List<CountryPharmacyModel> pharmacyEspecif() throws ExceptionGenericDataSource;

}
