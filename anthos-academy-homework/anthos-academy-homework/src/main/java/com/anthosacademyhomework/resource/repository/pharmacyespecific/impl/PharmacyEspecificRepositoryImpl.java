package com.anthosacademyhomework.resource.repository.pharmacyespecific.impl;

import com.anthosacademyhomework.resource.datasource.countrypharmacy.CountryPharmacyDataSource;
import com.anthosacademyhomework.resource.datasource.countrypharmacy.model.CountryPharmacyModel;
import com.anthosacademyhomework.resource.datasource.excepcion.ExceptionGenericDataSource;
import com.anthosacademyhomework.resource.repository.pharmacyespecific.PharmacyEspecifRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class PharmacyEspecificRepositoryImpl implements PharmacyEspecifRepository {
    @Autowired
    CountryPharmacyDataSource countryPharmacyDataSource;

    @Override
    public List<CountryPharmacyModel> pharmacyEspecif() throws ExceptionGenericDataSource {
        List<CountryPharmacyModel> listFilter=new ArrayList<>();
        List<CountryPharmacyModel> listPharmacy = countryPharmacyDataSource.pharmavalue();

        for (CountryPharmacyModel cont:listPharmacy
             ) {
            if (cont.getComunaNombre().equals("LOS ANGELES") || cont.getComunaNombre().equals("TALCA")|| cont.getComunaNombre().equals("PUERTO MONTT")|| cont.getComunaNombre().equals("AYSEN")){
                listFilter.add(cont);
            }

        }


        return listFilter;
    }
}
