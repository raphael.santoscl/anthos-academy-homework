package com.anthosacademyhomework.resource.datasource.countrypharmacy;

import com.anthosacademyhomework.resource.datasource.countrypharmacy.model.CountryPharmacyModel;
import com.anthosacademyhomework.resource.datasource.excepcion.ExceptionGenericDataSource;

import java.util.List;

public interface CountryPharmacyDataSource {
    List<CountryPharmacyModel> pharmavalue() throws ExceptionGenericDataSource;
}
