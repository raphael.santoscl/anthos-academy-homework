package com.anthosacademyhomework.resource.repository.pharmacyturn;

import com.anthosacademyhomework.resource.datasource.countrypharmacy.model.CountryPharmacyModel;
import com.anthosacademyhomework.resource.datasource.excepcion.ExceptionGenericDataSource;
import com.anthosacademyhomework.resource.datasource.pharmacyturn.model.PharmacyTurnModel;

import java.util.List;

public interface PharmacyTurnRepository {
    public List<PharmacyTurnModel> pharmacyEspecif() throws ExceptionGenericDataSource;
}
