package com.anthosacademyhomework.resource.repository.feriado;

import com.anthosacademyhomework.resource.datasource.countrypharmacy.model.CountryPharmacyModel;
import com.anthosacademyhomework.resource.datasource.excepcion.ExceptionGenericDataSource;
import com.anthosacademyhomework.resource.datasource.feriado.model.FeriadoValueModel;

import java.util.List;

public interface FeriadoRepository {
    FeriadoValueModel getAlLFeriado() throws ExceptionGenericDataSource;
}
