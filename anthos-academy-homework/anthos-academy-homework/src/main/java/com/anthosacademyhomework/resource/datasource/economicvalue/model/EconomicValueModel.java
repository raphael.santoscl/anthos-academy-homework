package com.anthosacademyhomework.resource.datasource.economicvalue.model;

public class EconomicValueModel {

    private String     version;
    private String     autor;
    private String     fecha;
    private DetalleEconomicModel uf;
    private DetalleEconomicModel dolar;
    private DetalleEconomicModel euro;
    private DetalleEconomicModel utm;
    private DetalleEconomicModel  bitcoin;

    public DetalleEconomicModel getUtm() {
        return utm;
    }

    public void setUtm(DetalleEconomicModel utm) {
        this.utm = utm;
    }

    public DetalleEconomicModel getBitcoin() {
        return bitcoin;
    }

    public void setBitcoin(DetalleEconomicModel bitcoin) {
        this.bitcoin = bitcoin;
    }

    public DetalleEconomicModel getEuro() {
        return euro;
    }

    public void setEuro(DetalleEconomicModel euro) {
        this.euro = euro;
    }

    public DetalleEconomicModel getDolar() {
        return dolar;
    }

    public void setDolar(DetalleEconomicModel dolar) {
        this.dolar = dolar;
    }



    public EconomicValueModel() {
    }

    public String getVersion() {
        return version;
    }

    public DetalleEconomicModel getUf() {
        return uf;
    }

    public void setUf(DetalleEconomicModel uf) {
        this.uf = uf;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
