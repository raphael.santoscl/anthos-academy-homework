package com.anthosacademyhomework.resource.repository.pharmacyturnespefic;

import com.anthosacademyhomework.resource.datasource.countrypharmacy.model.CountryPharmacyModel;
import com.anthosacademyhomework.resource.datasource.excepcion.ExceptionGenericDataSource;
import com.anthosacademyhomework.resource.datasource.pharmacyturn.model.PharmacyTurnModel;

import java.util.List;

public interface PharmacyTurnEspecificRepository {
    public List<PharmacyTurnModel> pharmacyEspecif() throws ExceptionGenericDataSource;
}
