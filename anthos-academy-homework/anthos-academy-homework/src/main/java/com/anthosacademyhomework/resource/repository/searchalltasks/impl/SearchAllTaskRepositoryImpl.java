package com.anthosacademyhomework.resource.repository.searchalltasks.impl;

import com.anthosacademyhomework.resource.datasource.createtask.CreateTaskDAO;
import com.anthosacademyhomework.resource.datasource.createtask.entity.CreateTaskEntity;
import com.anthosacademyhomework.resource.repository.searchalltasks.SearchAllTaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Repository
public class SearchAllTaskRepositoryImpl implements SearchAllTaskRepository {

    @Autowired
    private CreateTaskDAO searchAllCreateTask;

    @Override
    public List<CreateTaskEntity> getAllTask() {
        Iterable<CreateTaskEntity> iterable = searchAllCreateTask.findAll();
        List<CreateTaskEntity> TaskList = StreamSupport.stream(iterable.spliterator(), false)
                .collect(Collectors.toList());

        return TaskList;

    }
}
