package com.anthosacademyhomework.resource.repository.countrypharmacy;

import com.anthosacademyhomework.resource.datasource.countrypharmacy.model.CountryPharmacyModel;
import com.anthosacademyhomework.resource.datasource.excepcion.ExceptionGenericDataSource;

import java.util.List;

public interface  CountryPharmacyRepository {

    List<CountryPharmacyModel> getAllEPharmacy() throws ExceptionGenericDataSource;

}
