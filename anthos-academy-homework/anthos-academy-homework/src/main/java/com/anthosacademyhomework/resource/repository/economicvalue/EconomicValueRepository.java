package com.anthosacademyhomework.resource.repository.economicvalue;

import com.anthosacademyhomework.resource.datasource.economicvalue.model.EconomicValueModel;
import com.anthosacademyhomework.resource.datasource.excepcion.ExceptionGenericDataSource;

import java.util.List;
import java.util.Optional;

public interface EconomicValueRepository {
    EconomicValueModel getAllEconomic() throws ExceptionGenericDataSource;
}
