package com.anthosacademyhomework.controller;

import com.anthosacademyhomework.resource.datasource.excepcion.ExceptionGenericDataSource;
import com.anthosacademyhomework.resource.datasource.feriado.FeriadoDataSource;
import com.anthosacademyhomework.resource.datasource.feriado.model.FeriadoValueModel;
import com.anthosacademyhomework.usecase.feriado.FeriadoUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class HolidayController {

    @Autowired
    FeriadoUseCase feriadoUseCase;

@GetMapping(path = "/feriado")
    public FeriadoValueModel BuscarFeriado() throws ExceptionGenericDataSource {


       return feriadoUseCase.getFeriado();

    }




}
