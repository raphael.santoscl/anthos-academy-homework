package com.anthosacademyhomework.controller;


import com.anthosacademyhomework.resource.datasource.countrypharmacy.model.CountryPharmacyModel;
import com.anthosacademyhomework.resource.datasource.excepcion.ExceptionGenericDataSource;
import com.anthosacademyhomework.resource.repository.pharmacyespecific.PharmacyEspecifRepository;
import com.anthosacademyhomework.usecase.pharmacyespecific.PharmacyEspecificUse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class PharmaEspecificController {
    @Autowired
    PharmacyEspecificUse pharmacyEspecificUse;

    @GetMapping(path = "/pharmacy-especific")

    public List<CountryPharmacyModel> BuscarValores() throws ExceptionGenericDataSource {

        return pharmacyEspecificUse.getPharmacyEspecific();
    }
}
