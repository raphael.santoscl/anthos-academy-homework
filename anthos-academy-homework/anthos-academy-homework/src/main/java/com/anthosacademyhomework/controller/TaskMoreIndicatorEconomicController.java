package com.anthosacademyhomework.controller;

import com.anthosacademyhomework.resource.datasource.economicvalue.model.EconomicValueModel;
import com.anthosacademyhomework.resource.datasource.excepcion.ExceptionGenericDataSource;
import com.anthosacademyhomework.usecase.alltaskmoreeconomic.AllTaskMoreEconomicUseCase;
import com.anthosacademyhomework.usecase.alltaskmoreeconomic.model.AllTaskEconomicIndicatorModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class TaskMoreIndicatorEconomicController {
    @Autowired
    AllTaskMoreEconomicUseCase allTaskEconomicIndicatorModel;



    @GetMapping(path = "/all-task-indicator")
    public AllTaskEconomicIndicatorModel getAllTaskEconomicIndicatorModel() throws ExceptionGenericDataSource {
        return allTaskEconomicIndicatorModel.allTaskEconomicIndicatorModel() ;
    }


}
