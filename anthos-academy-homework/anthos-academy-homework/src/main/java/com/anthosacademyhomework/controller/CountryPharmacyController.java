package com.anthosacademyhomework.controller;

import com.anthosacademyhomework.resource.datasource.countrypharmacy.model.CountryPharmacyModel;
import com.anthosacademyhomework.resource.datasource.excepcion.ExceptionGenericDataSource;
import com.anthosacademyhomework.usecase.countrypharmacy.countrypharmacyUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")

public class CountryPharmacyController {
    @Autowired
    private countrypharmacyUseCase countrypharmacyUseCase;

    @GetMapping(path = "/pharmacy")

    public List<CountryPharmacyModel> BuscarValores() throws ExceptionGenericDataSource {

        return countrypharmacyUseCase.getCountryPharmacyModel();
    }



}
