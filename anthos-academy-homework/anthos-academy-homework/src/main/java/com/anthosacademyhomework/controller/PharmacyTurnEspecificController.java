package com.anthosacademyhomework.controller;

import com.anthosacademyhomework.resource.datasource.countrypharmacy.model.CountryPharmacyModel;
import com.anthosacademyhomework.resource.datasource.excepcion.ExceptionGenericDataSource;
import com.anthosacademyhomework.resource.datasource.pharmacyturn.model.PharmacyTurnModel;
import com.anthosacademyhomework.usecase.pharmacyespecific.PharmacyEspecificUse;
import com.anthosacademyhomework.usecase.pharmacyturnespecifc.PharmacyTurnEspecificUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;



@RestController
@RequestMapping("/api")
public class PharmacyTurnEspecificController {
    @Autowired
    PharmacyTurnEspecificUseCase pharmacyEspecificTurnUse;

    @GetMapping(path = "/pharmacy-especific-turn")

    public List<PharmacyTurnModel> BuscarValores() throws ExceptionGenericDataSource {

        return pharmacyEspecificTurnUse.getPharmacyEspecific();
    }
}
