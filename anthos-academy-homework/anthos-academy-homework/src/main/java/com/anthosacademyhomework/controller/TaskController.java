package com.anthosacademyhomework.controller;

import com.anthosacademyhomework.resource.datasource.createtask.entity.CreateTaskEntity;
import com.anthosacademyhomework.usecase.createtask.CreateTasKUseCase;
import com.anthosacademyhomework.usecase.deletetask.DeleteTaskUseCase;
import com.anthosacademyhomework.usecase.searchall.SearcAllTaskUseCase;
import com.anthosacademyhomework.usecase.updatetask.UpdateTaskUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class TaskController {

    @Autowired
    private CreateTasKUseCase saveCreateTask;

    @Autowired
    private SearcAllTaskUseCase searchAllTask1;


    @Autowired
    private DeleteTaskUseCase deleteTask;
@Autowired
private UpdateTaskUseCase updateTaskUseCase;

    @PostMapping(path = "/save-task")
    public void saveTask(@RequestBody CreateTaskEntity createTask) {
        saveCreateTask.execute(createTask);
    }


    @GetMapping(path = "/all-task")
    public List<CreateTaskEntity> GetAllTask() {
        return searchAllTask1.searchaAllTask();
    }

    @DeleteMapping(path = "/delete-task/{id}")
    public void deleteTaskUseCase(@PathVariable Long id) {
        deleteTask.DeleteTask(id);
    }


    @PutMapping(path = "/update-task/{id}")

        public void updateTaskUseCase(@RequestBody CreateTaskEntity updateTask) {
        updateTaskUseCase.execute(updateTask);

        }
    }



