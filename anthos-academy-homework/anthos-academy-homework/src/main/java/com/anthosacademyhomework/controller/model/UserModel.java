package com.anthosacademyhomework.controller.model;

public class UserModel {

    private String user;
    private String pass;
    private String token;

    public String getUser() {
        return user;
    }

    public String getPass() {
        return pass;
    }

    public String getToken() {
        return token;
    }

    public static UserModelBuilder builder() {
        return new UserModelBuilder();
    }
    public static final class UserModelBuilder {
        private String user;
        private String pass;

        private String token;

        private UserModelBuilder() {
        }

        public UserModelBuilder withUser(String user) {
            this.user = user;
            return this;
        }

        public UserModelBuilder withPass(String pass) {
            this.pass = pass;
            return this;
        }

        public UserModelBuilder withToken(String token) {
            this.token = token;
            return this;
        }

        public UserModel build() {
            UserModel userModel = new UserModel();
            userModel.token = this.token;
            userModel.pass = this.pass;
            userModel.user = this.user;
            return userModel;
        }
    }
}