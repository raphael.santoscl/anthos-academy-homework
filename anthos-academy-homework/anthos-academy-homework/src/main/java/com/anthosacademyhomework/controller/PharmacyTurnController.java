package com.anthosacademyhomework.controller;

import com.anthosacademyhomework.resource.datasource.excepcion.ExceptionGenericDataSource;
import com.anthosacademyhomework.resource.datasource.pharmacyturn.model.PharmacyTurnModel;
import com.anthosacademyhomework.usecase.pharmacyturn.PharmacyTurnUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class PharmacyTurnController {
    @Autowired
    private PharmacyTurnUseCase pharmacyTurnUseCase;

    @GetMapping(path = "/pharmacy-turn")

    public List<PharmacyTurnModel> BuscarValores() throws ExceptionGenericDataSource {

        return pharmacyTurnUseCase.pharmacyTurnModels();
    }
}
