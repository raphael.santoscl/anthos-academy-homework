package com.anthosacademyhomework.controller;


import com.anthosacademyhomework.resource.datasource.economicvalue.model.EconomicValueModel;
import com.anthosacademyhomework.resource.datasource.excepcion.ExceptionGenericDataSource;
import com.anthosacademyhomework.usecase.economicvalue.EconomicValueUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class FinanceController {

    @Autowired
    private EconomicValueUseCase obtencionEconomicValueUseCase;


    @GetMapping(path = "/finance")
    public EconomicValueModel BuscarValores() throws ExceptionGenericDataSource {
        return obtencionEconomicValueUseCase.getalleconomic();
    }

}
